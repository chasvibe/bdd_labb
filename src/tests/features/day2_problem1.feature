Feature: A program that will look through every item from a
         spreadsheet and compare the highest with the lowest
         and add the difference to the total number

    Scenario Outline: Get the sum of the spreadsheet
        Given A <sequence> of numbers from a spreadsheet

        When comparing the highest with the lowest

        Then add the <difference> between those to the total

        Examples:
          | sequence | difference |
          | 5195     | 8          |
          | 753      | 4          |
          | 2468     | 6          |


        # pytest-bdd generate <feature file> .. <test file>
