Feature: Reversed captcha
    A reversed captcha to be solved by adding numbers of a sequence together

    Scenario Outline: Solve the captcha
        Given A <sequence> of numbers

        When I add each number in the sequence with the next

        Then I should get <result>

        Examples:
        | sequence | result |
        |   1122   |    3   |
        |   1111   |    4   |
        |   1234   |    0   |
        | 91212129 |    9   |


        # pytest-bdd generate <feature file> .. <test file>
