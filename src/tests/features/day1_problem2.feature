Feature: Reversed captcha 2
    A reversed captcha to be solved by adding numbers of a sequence together,
    but only if the letter appears halfway through the list from where th number
    being compared is

    Scenario Outline: Solve the captcha again
        Given A <sequence> of numbers

        When I add each number in the sequence

        Then I should get <result>

        Examples:
        | sequence | result |
        |   1212   |    6   |
        |   1221   |    0   |
        |  123425  |    4   |
        |  123123  |   12   |
        | 12131415 |    4   |


        # pytest-bdd generate <feature file> .. <test file>
