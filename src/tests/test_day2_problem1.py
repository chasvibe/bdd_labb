"""tests/test_day2_problem1.py feature tests."""

from pytest_bdd import (
    given,
    scenario,
    then,
    when,
    parsers
)
from ..day2_problem1 import problem_1, sequence_converter


test_input = "123  847     23555 8488"


def test_sequence_converter():
    """ Test for a unforseen sequence converter """
    assert sequence_converter(test_input) == [123, 847, 23555, 8488]


@scenario('features/day2_problem1.feature', 'Get the sum of the spreadsheet')
def test_get_the_sum_of_the_spreadsheet():
    """Get the sum of the spreadsheet."""


@given(parsers.parse('A {sequence} of numbers from a spreadsheet'), target_fixture="puzzle_input")
def a_sequence_of_numbers_from_a_spreadsheet(sequence):
    """A <sequence> of numbers from a spreadsheet."""
    return sequence


@when(parsers.parse('comparing the highest with the lowest'), target_fixture="diff")
def comparing_the_highest_with_the_lowest(puzzle_input):
    """comparing the highest with the lowest."""
    return problem_1(puzzle_input)


@then(parsers.parse('add the {difference:d} between those to the total'))
def add_the_difference_between_those_to_the_total(diff, difference):
    """add the <difference> between those to the total."""
    assert diff == difference
