"""tests/test_day1_problem1.py feature tests."""

from pytest_bdd import (
    given,
    scenario,
    then,
    when,
    parsers
)
from ..day1_problem1 import problem_1


@scenario('features/day1_problem1.feature', 'Solve the captcha')
def test_solve_the_captcha():
    """Solve the captcha."""


@scenario('features/day1_problem1.feature', 'Solve the captcha')
def test_solve_the_captcha():
    """Solve the captcha."""


@scenario('features/day1_problem1.feature', 'Solve the captcha')
def test_solve_the_captcha():
    """Solve the captcha."""


@given(parsers.parse('A {sequence} of numbers'), target_fixture='puzzle_input')
def a_sequence_of_numbers(sequence):
    """A <sequence> of numbers."""
    return sequence


@when('I add each number in the sequence with the next', target_fixture='puzzle_result')
def i_add_each_number_in_the_sequence_with_the_next(puzzle_input):
    """I add each number in the sequence with the next."""
    return problem_1(puzzle_input)


@then(parsers.parse('I should get {result:d}'))
def i_should_get_result(puzzle_result, result):
    """I should get <result>."""
    assert puzzle_result == result
