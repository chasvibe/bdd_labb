from pathlib import Path


def problem_1(seq):
    result = 0
    for index, i in enumerate(seq):
        if i == seq[(index + 1) % len(seq)]:
            result += int(i)
    return result


if __name__ == '__main__':  # pragma: no cover
    input_file = Path.home() / 'chas2022/bdd_labb/src/input.txt'
    with input_file.open(mode='r') as file:
        data = file.read()
    print(problem_1(data))
