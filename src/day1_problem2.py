from pathlib import Path


def problem_2(seq):
    result = 0
    for index, i in enumerate(seq):
        if i == seq[(index + (len(seq) // 2)) % len(seq)]:
            result += int(i)
    return result


if __name__ == '__main__':  # pragma: no cover
    input_file = Path.home() / 'chas2022/bdd_labb/src/input.txt'
    with input_file.open(mode='r', encoding='utf-8') as file:
        data = file.readline()
    print(problem_2(data))
